(function($) {
  'use strict';

  function $http(options, data) {
      let params = $.extend({
          type: 'POST',
          contentType: 'application/json;charset=UTF-8'
      }, options);

      if (data) {
          params.data = JSON.stringify(data);
      }

      return new Promise((resolve, reject) => {
          $.ajax(params)
              .done(resolve)
              .fail(reject);
      });
  }

  function getRestApi(url) {

      return $http({
          type: 'GET',
          url: `${url}`
      });
  }

  function postRestApi(url,params){

      return $http({
            type: 'POST',
            url: `${url}`
        }, params);
  }

  window.api = {
    getRestApi:getRestApi,
    postRestApi:postRestApi
  }

})(jQuery);
