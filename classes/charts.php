<?php
/**
 *
 */
class MSCharts
{
  public $args;
  public $maxHeight;
  public $maxWidth;
  public $unit;
  public $barHeight;
  public $barWidth;
  public $data;
  public $y;
  public $x;

  function __construct( $args )
  {
    if( !is_array( $args ) ){
      $args["maxHeight"]  = 100;
      $args["maxWidth"]   = 400;
      $args["barHeight"]  = 50;
      $args["barWidth"]   = 30;
      $args["unit"]       = 10;
      $args["data"]       = array(22, 30);
    }
    $this->args = $args;
    $this->maxHeight    = isset($this->args["maxHeight"]) ? $this->args["maxHeight"] : NULL;
    $this->maxWidth     = isset($this->args["maxWidth"])  ? $this->args["maxWidth"]  : NULL;
    $this->barHeight    = isset($this->args["barHeight"]) ? $this->args["barHeight"] : NULL;
    $this->barWidth     = $this->args["barWidth"];
    $this->unit         = $this->args["unit"];

    $this->data = $this->args["data"];
    $this->y    = $this->args["y"]["col"];
    $this->x    = $this->args["x"]["col"];
  }

  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
  public function items(){
    $x = $this->x;
    $y = $this->y;
    $out = '';
    /*var_dump($data);*/
    if( count( $this->data ) > 0 ){
      foreach( $this->data as $d){
        $out .= '<div style="height: '.round($d->$y/$this->unit).'px; width:'.$this->barWidth.'px" class="bar"><span class="valueX">'.$d->$x.'</span><span class="valueY">'.$d->$y.'</span></div>';
      }
    }
    return $out;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */

  public function bar()
  {
    $out = '<div class="ms-graph">';
    $out .= '<div class="chart-bar" style="height:'.($this->maxHeight+30).'px;min-width:'.($this->maxWidth).'px;">';
    $out .= '<div class="chart-table clearfix">';
    $out .= $this->items();
    $out .= '</div>';
    $out .= '</div>';
    $out .= '</div>';
    return $out;
  }
}

?>
