<?php
/**
 *
 */
class MSInput
{

  public static function display( $args )
	{
		$disable      = '';
    $icon         = '';
    $description  =  '';
    $args = isset($args) && is_array($args) ? $args : NULL;

		if( isset($args["disable"]) && $args["disable"] == true){
			$disable = 'disabled';
		}
		if( isset($args["icon"]) && is_array( $args["icon"] ) )
		{
			$icon = ' <a href="#" title="'. $args["icon"][1] .'"><span class="dashicons '.$args["icon"][0].'"></span></a>';
		}

    if(  isset($args["description"]) && $args["description"] )
    {
      $description = '<br>' . $args["description"];
    }

    if(@$args["type"] == "empty"){
			return $args["value"];
		}


		if(@$args["type"] == "checkbox"){
			return '<input type="checkbox" name="'.@$args["name"].'" value="1"class="'.@$args["class"].'" '.((@$args["value"] == 1) ? 'checked' : '').'>' . $icon;
		}

		if(@$args["type"] == "select"){
			$out = '';
			$out .= '<select name="'.@$args["name"].'" class="'.@$args["class"].'">';

			foreach( $args["options"] as $optk => $optv ){
				$out .= '<option value="'.$optv.'" '. ( (isset($args["value"]) && $optv == $args["value"] ) ? 'selected' : '' ) .'>' . $optv . '</option>';
			}
			$out .= '</select>' . $icon;
			return $out;
		}
    if(@$args["type"] == "hidden"){
      return '<input type="hidden" name="'.@$args["name"].'" value="'.@$args["value"].'">';
    }

    if(@$args["type"] == "hiddenShowValue"){
      return '<input type="hidden" name="'.@$args["name"].'" value="'.@$args["value"].'" class="'.@$args["class"].'">'. @$args["value"];
    }

		return '<input type="text" name="'.@$args["name"].'" value="'.@$args["value"].'" class="'.@$args["class"].'" style="width:'.((@$args["icon"]) ? '85%' : '100%').'" '.$disable.'>'. $icon . $description;
	}
}

?>
